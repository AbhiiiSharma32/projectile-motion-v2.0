import Project.DrawingPanel;

public interface Shoot {
    public Double[] Calculate(Double[] i);
    public DrawingPanel Panel();
    public void ShootTheShot(Double[] a, Double[] b, DrawingPanel c);
    public void TieTogether();
    
}
